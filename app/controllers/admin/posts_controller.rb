class Admin::PostsController < ApplicationController
  prepend_before_action :authenticate_user!

  def index
    @user = current_user.id
    @posts = Post.all
  end

  def admin_post
    @user = current_user.id
    if params[:id] != nil
      @post = Post.find(params[:id])
    end
  end

  def create_or_update
    if params[:id] != nil
      @post = Post.find(params[:id])
      @post.update_attributes(:title => params[:title], :content => params[:inquiry])

      if(params[:image] != nil)
        @post.update_attributes(:image => params[:image])
      end

      if @post.save()
        redirect_to admin_posts_path()
      else
        redirect_to request.referrer, error: 'Article is successfully created'
      end

    else
      @post = Post.new(:title=>params[:title], :image=>params[:image], :content=>params[:inquiry], :users_id => params[:current_user])
      if @post.save
        redirect_to admin_posts_path(), notice: "OMG IT'S WORKING"
      else
      end
    end
  end
end
