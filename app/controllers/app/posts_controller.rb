class App::PostsController < ApplicationController
  def index
    @posts = Post.limit(5)
  end

  def show
    @post = Post.find(params[:id])
  end

  def archive
    @posts = Post.paginate(:page => params[:page], :per_page => 5)
  end
end
