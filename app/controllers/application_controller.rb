class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session
  add_flash_types :success, :error

  def after_sign_in_path_for(resource)
    admin_posts_path
  end

end
