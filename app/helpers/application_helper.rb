module ApplicationHelper
  def custom_rederer
    Class.new(WillPaginate::ActionView::LinkRenderer) do
      def container_attributes
        {class: "paginate"}
      end

      def page_number(page)
        if page == current_page
          tag(:span, page, class: 'paginate-number is-current')
        else
          link(page, page, class: 'paginate-number', rel: rel_value(page))
        end
      end

      def gap
        text = @template.will_paginate_translate(:page_gap) { '&hellip;' }
        %(<span class="mr2">#{text}</span>)
      end

      def previous_page
        num = @collection.current_page > 1 && @collection.current_page - 1
        previous_or_next_page(num, 'paginate-prev')
      end

      def next_page
        num = @collection.current_page < total_pages && @collection.current_page + 1
        previous_or_next_page(num, 'paginate-next')
      end

      def previous_or_next_page(page, classname)
        arrow = ''
        if classname == 'paginate-next'
          arrow = '%(<span class="paginate-next-arrow"></span>)'
        else
          arrow = '%(<span class="paginate-prev-arrow"></span>)'
        end

        if page
          link(arrow ,page, :class => classname)
        else
          tag(:span, arrow , :class => classname + ' is-disable')
        end
      end
    end
  end
end
