class Post < ApplicationRecord
  has_attached_file :image
  validates :title, :content, :image, presence: true
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end
