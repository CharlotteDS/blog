class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  validates :user_id, uniqueness: true

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, authentication_keys: [:user_id]

  has_many :posts
end
