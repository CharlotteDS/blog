Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root :to => 'app/posts#index'

  namespace :admin do
    resources :posts
    get 'admin_post/(:id)', :to => 'posts#admin_post'
    post 'create_or_update/:current_user/(:id)', :to => 'posts#create_or_update'
  end

  namespace :app do
    resources :posts
    get 'archive', :to => 'posts#archive'
  end

  devise_for :users, controllers: {
       sessions: 'admin/sessions',
       skip: :registrations
  }

  get 'admin', :to => 'admin/posts#index'
end
